#ifndef STRIBOG_H
#define STRIBOG_H

#include <stdint.h>
#include <string.h>
#define BLOCK_SIZE 64 // Размер блока 64 байт (или 512 бит)

typedef uint8_t vector[BLOCK_SIZE];

typedef struct Context
{
  vector buffer; // Буфер для очередного блока хешируемого сообщения / по факту вектор m
  vector hash; // Итоговый результат вычислений
  vector h; // Промежуточный результат вычислений
  vector N; //
  vector Sigma; //Контрольная сумма
  vector v_0; // Инициализационный вектор
  vector v_512; // Инициализационный вектор
  unsigned int buf_size; // Размер оставшейся части сообщения
                   // (которая оказалась меньше очередных 64 байт)
  int hash_size;   // Размер хеш-суммы (512 или 256 бит)
} Context;

//Основные преобразования

//X-преобразование // она же ксор
void stribog_X(vector first, // входной вектор
                  vector second, // входной вектор
                  vector result);  // результат

//S-преобразование
void stribog_S(vector input); // входной вектор

//P-преобразование
void stribog_P(vector input); // входной вектор

//L-преобразование
void stribog_L(vector input); // входной вектор

//операция сложения в кольце Z2^n // она же исключающее или
void stribog_Add(vector first, // входной вектор
                    vector second, // входной вектор
                    vector result); // результат

//Генерация раундового ключа
void stribog_GetKey(vector K, // ключ
                       int i); // номер итерации

//Функция сжатия

//Функция E
void stribog_E(vector K, // ключ
                  vector input, // входной вектор
                  vector output); // результат

//Функция сжатия G
void stribog_G(vector h, // вектор h
                  vector N, // вектор N
                  vector m); // входной блок сообщения

//Алгоритм

//Дополнение блока к виду 00 .. 00 01 ..
void stribog_Padding(vector input, unsigned int size); // состаяние для обробатываемого блока

//Этап 1 //Инициализация базовой структуры для результата в 512 бит // IV 00 00 ...
void stribog_Init512(Context *Context); // сама структура

//Этап 1 //Инициализация базовой структуры для результата в 256 бит // IV 01 01 ...
void stribog_Init256(Context *Context); // сама структура

//Этап 2
void stribog_2(Context *Context); // состояние пред этапом

//Этап 3
void stribog_3(Context *Context); // состояние пред этапом

//Хеширование с результатом в 512 бит
uint8_t* stribog_512(uint8_t data[]);//входное сообщение/файл

//Хеширование с результатом в 256 бит
uint8_t* stribog_256(uint8_t data[]);//входное сообщение/файл
#endif // STRIBOG_H
