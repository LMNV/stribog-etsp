#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "stribog.h"
#include "eccrypt.h"
#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
//#include <QByteArray>
#include <stdio.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    eccrypt_curve_t EC; // параметры кривой

    //eccrypt_point_t P; // точка эллиптической кривой

    // вектора для формирования подписи

    bignum_digit_t Signature_key[ECCRYPT_BIGNUM_DIGITS]; // ключ подписи, он же вектор d из ГОСТа

    eccrypt_point_t Q; // ключ проверки подписи

    bignum_digit_t alpha[ECCRYPT_BIGNUM_DIGITS]; // вектор альфа, векторное представление хэша сообщения

    bignum_digit_t e[ECCRYPT_BIGNUM_DIGITS]; // вектор e = альфа mod q, где q - порядок циклической подгруппы

    bignum_digit_t k[ECCRYPT_BIGNUM_DIGITS]; // случайно сгенерированное число, причем 0<k<q,

    eccrypt_point_t C; // точка С=kP

    bignum_digit_t r[ECCRYPT_BIGNUM_DIGITS]; // вектор r = C.x mod q, где C.x - координата x точки C

    bignum_digit_t s[ECCRYPT_BIGNUM_DIGITS]; // вектор s = (rd+ke) mod q

    // вектора для проверки подписи

    bignum_digit_t v[ECCRYPT_BIGNUM_DIGITS]; // вектор v = e^(-1) mod q

    bignum_digit_t z1[ECCRYPT_BIGNUM_DIGITS]; // вектор z1 = sv mod q

    bignum_digit_t z2[ECCRYPT_BIGNUM_DIGITS]; // вектор z2 = -rv mod q

    bignum_digit_t R[ECCRYPT_BIGNUM_DIGITS]; // вектор R = C.x mod q, в данном случае точка C расчитывается как C=z1P+z2Q

    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_Elliptic_curve_param_accept_clicked(); // принять параметры кривой

    void on_Signature_key_accept_clicked(); // принять ключ подписания

    void on_Open_file_clicked(); // открыть файл

    void on_Calculate_EDS_clicked(); // вычислить подпись

    void on_Check_EDS_clicked(); // проверить подпись

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
